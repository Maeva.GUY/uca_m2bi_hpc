# HPC Project - Maëva GUY

## Prerequisites

To run the scripts, there are 2 possibilities:
- __Option 1__: Clone this git in your home using the command line: <br>$ git clone git@gitlab.com:Maeva.GUY/uca_m2bi_hpc.git

- __Option 2__: Move in my directory using:<br> $ cd /home/users/student17/uca_m2bi_hpc

If you chose the first option, please make sure to adapt the __#SBATCH -o__ file path at the top of the scripts to your own working environment.

If option 1 does not work, please try option 2 which should be working

## STATegra


The ATAC-seq, or "Assay for Transposase-Accessible Chromatin with high-throughput sequencing," is a groundbreaking technique in the field of functional genomics. It allows the analysis of chromatin structure by identifying regions accessible to transcription factors and characterizing changes in chromatin conformation associated with different cellular states.

STATegra by Gomez-Cabrero et al. 2019 (https://doi.org/10.1038/s41597-019-0202-7) 
is a multidisciplinary project aimed at integrating data from various omics technologies to better understand the dynamics of biological regulatory networks associated with B-cell
diferentiation in mouse. <br>
The project focuses on integrating data from diverse sources such as genomics, transcriptomics, proteomics, and metabolomics (__RNA-seq, miRNA-seq, RRBS, DNase-seq, scRNA-seq, ATAC-seq, matabolomics and proteomics__) <br>

Here, we will be providing the first steps of the __ATAC-seq__ analysis
(informations available at: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE89362)

## ATAC-seq analysis

2 Conditions were analysed, 0h and 24h with 3 replicats each :
- 0h 
    - __SRR4785152__  50k-Rep1-0h-sample.0h : 50k_0h_R1
    - __SRR4785153__  50k-Rep2-0h-sample.0h : 50k_0h_R2
    - __SRR4785154__  50k-Rep3-0h-sample.0h : 50k_0h_R3

- 24h
    - __SRR4785341__  50k-24h-R1-sample.24h.2 : 50k_24h_R1
    - __SRR4785342__  50k-24h-R2-sample.24h.2 : 50k_24h_R2
    - __SRR4785343__  50k-24h-R3-sample.24h.2 : 50k_24h_R3

## How to run

There are two ways to run the whole analysis :
- By using the *ATAC-seq_analysis.sh* script that allows all scripts to run
- By running scripts independantly. However, if this method is chosen, you need to be aware of the importance of running the scripts in the correct following order


The first script *0_environment_prep.slurm* 


